import vk
import random
import settings
import requests
import json

session = vk.Session()
api = vk.API(session, v=5.0)


def get_random_wall_picture(group_id):
    max_num = api.photos.get(owner_id=group_id, album_id='wall', count=0)['count']
    num = random.randint(1, max_num)
    photo = api.photos.get(owner_id=str(group_id), album_id='wall', count=1, offset=num)['items'][0]['id']
    attachment = 'photo' + str(group_id) + '_' + str(photo)
    return attachment


def send_message(user_id, token, message, attachment=""):
    api.messages.send(access_token=token, user_id=str(user_id), message=message, attachment=attachment)


def save_file(user_id, file):
    import json
    upload_server_response = api.docs.getMessagesUploadServer(access_token=settings.token,
                                                              peer_id=user_id, type='doc')
    file_ = {'file': (file, open(file, mode='r'))}
    r = requests.post(upload_server_response['upload_url'], files=file_)
    s = r.text
    s = json.loads(s)
    save = api.docs.save(access_token=settings.token, file=s['file'], title='Расписание 3 курс 1 семестр')
    f = save[0]['url']
    t = ''
    ind = 0
    ind2 = 0
    for x in range(len(f)):
        if f[x] == 'd' and f[x + 1] == 'o' and f[x + 2] == 'c':
            ind = x
        if f[x] == '?':
            ind2 = x
            break
    f = f[ind:ind2]
    # attachment = 'doc' + str(user_id) + '_' + str(photo)
    return (f)
