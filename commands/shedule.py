from datetime import *
import command_system
import json
import os
import vkapi
import calendar
import pytz


def readfile(day):
    now = datetime.now(tz=pytz.timezone('Europe/Moscow')) + timedelta(days=day)
    now.replace(hour=0, minute=0, second=0, microsecond=0)
    if now.month > 8:
        year, next_year = now.year, now.year + 1
    if now.month < 7:
        year, next_year = now.year - 1, now.year
    w = 0
    for m in range(9, 13):
        c = calendar.monthcalendar(year, m)
        flag = 0
        for x in c:
            if m > 9 and 0 not in x and c[0] == x:
                w += 1
            if m == 9:
                w += 1
            if x != c[0] and m > 9:
                w += 1
            if now.day in x and m == now.month:
                flag = 1
                break
        if flag:
            break
    if now.year == next_year:
        for m in range(1, 7):
            c = calendar.monthcalendar(next_year, m)
            flag = 0
            for x in c:
                if 0 not in x and x == c[0]:
                    w += 1
                if x != c[0]:
                    w += 1
                if now.day in x and m == now.month:
                    flag = 1
                    break
            if flag:
                break
    week = w
    week_day = now.weekday() + 1
    try:
        cwd = os.getcwd() + '/mysite/commands/'
        with open(os.path.join(cwd, "shedule.json"), mode='r', encoding='utf-8') as file:
            s = file.read()
            s = s.replace('\n', '')
            s = json.loads(s)[str(week_day)]
            t = ''
            for x in s:
                if len(s[x]) == 3 and s[x]['subject'] != '':
                    t += s[x]['time'] + ' ' + s[x]['subject'] + ' ' + s[x]['room'] + '\n'
                if len(s[x]) == 2:
                    if week % 2 and s[x][0]:
                        t += s[x][0]['time'] + ' ' + s[x][0]['subject'] + ' ' + s[x][0]['room'] + '\n'
                    if s[x][1] and not week % 2:
                        t += s[x][1]['time'] + ' ' + s[x][1]['subject'] + ' ' + s[x][1]['room'] + '\n'
            if not week % 2:
                week = 'знаменатель'
            else:
                week = 'числитель'
            message = (week, t)
    except Exception as err:
        message = 'error! %s: %s' % (type(err), err)
    return message


def shedule():
    now = datetime.now(tz=pytz.timezone('Europe/Moscow'))
    week_day = now.weekday() + 1
    if 9 > now.month > 6 or week_day not in range(6):
        message = 'Я отдыхаю, отдохни и ты!'
    else:
        message = readfile(0)
    return '{0}\n{1}'.format(message[0], message[1]), ''


def tshedule():
    now = datetime.now(tz=pytz.timezone('Europe/Moscow')) + timedelta(days=1)
    week_day = now.weekday() + 1
    if 9 > now.month > 6 or week_day not in range(6):
        message = 'Я отдыхаю, отдохни и ты!'
    else:
        message = readfile(1)
    return 'Завтра\n{0}\n{1}'.format(message[0], message[1]), ''


def week_shedule():
    now = datetime.now(tz=pytz.timezone('Europe/Moscow'))
    week_day = now.weekday() + 1
    if 9 > now.month > 6:
        message = 'Я отдыхаю, отдохни и ты!'
    if 7 >= week_day >= 6:
        delta = 8 - week_day
    if week_day in range(6):
        delta = 1 - week_day
    message = ''
    day = {'1': 'Понедельник', '2': 'Вторник', '3': 'Среда', '4': 'Четверг', '5': 'Пятница'}
    for i in range(5):
        message += day[str(i + 1)] + '\n'
        message += readfile(i + delta)[1]
        message += '\n\n'

    return '{0}\n{1}'.format(readfile(delta)[0], message), ''


def printshedule():
    try:
        cwd = os.getcwd() + '/mysite/commands/'
        with open(os.path.join(cwd, "shedule.json"), mode='r', encoding='utf-8') as file:  #
            s = file.read()
            s = s.replace('\n', '')
            s = json.loads(s)
            exp = open(os.path.join(cwd, "shedule.txt"), mode='w')
            exp.write('%s;%s;%s;%s\n' % ('День', 'Время', 'Предмет', 'аудитория'))
            t = ''
            day = {'1': 'Понедельник', '2': 'Вторник', '3': 'Среда', '4': 'Четверг', '5': 'Пятница'}
            for x in s:
                for y in s[x]:
                    if len(s[x][y]) == 3 and s[x][y]:
                        exp.write('%s;%s;%s;%s\n' % (day[x], s[x][y]['time'], s[x][y]['subject'], s[x][y]['room']))
                    if len(s[x][y]) == 2:
                        if s[x][y][0]:
                            exp.write('%s;%s;%s;%s\n' % (
                            day[x], s[x][y][0]['time'], s[x][y][0]['subject'], s[x][y][0]['room'] + ' Числитель'))
                        if s[x][y][1]:
                            exp.write('%s;%s;%s;%s\n' % (
                            day[x], s[x][y][1]['time'], s[x][y][1]['subject'], s[x][y][1]['room'] + ' Знаменатель'))
                exp.write('\n')
            message = t
            exp.close()
            # 101378772/100458394/-101378772
            message = vkapi.save_file(100458394, os.path.join(cwd, "shedule.txt"))
    except Exception as err:
        message = 'error! %s: %s' % (type(err), err)
    return 'Расписание 3 курс 2 семестр', message


shedule_command = command_system.Command()
shedule_command.keys = ['расписание', 'shedule']
shedule_command.description = 'Покажу расписание'
shedule_command.process = shedule

shedulet_command = command_system.Command()
shedulet_command.keys = ['расписание на завтра', 'shedule for tomorrow', 'завтра', 'tomorrow']
shedulet_command.description = 'Покажу расписание на завтра'
shedulet_command.process = tshedule

shedule_command_week = command_system.Command()
shedule_command_week.keys = ['расписание на неделю', 'shedule for week', 'неделя', 'week']
shedule_command_week.description = 'Покажу расписание на неделю'
shedule_command_week.process = week_shedule

# test = command_system.Command()
# test.keys = ['test']
# test.process = printshedule
